import React from "react";

import Hero from "../components/Hero";
import Carousel from "../components/Carousel";
import Solutions from "../components/Solutions";
import Technologies from "../components/Technologies";

function HomePage(props) {
  return (
    <div>
      <Hero title={props.title} subTitle={props.subTitle} text={props.text} />
      <Solutions />
      <Carousel />
      <Technologies />
    </div>
  );
}

export default HomePage;
