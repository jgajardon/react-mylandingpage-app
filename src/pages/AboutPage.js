import React from "react";
import Hero from "../components/Hero";
import Content from "../components/Content";

function AboutPage(props) {
  return (
    <div>
      <Hero title={props.title} />

      <Content>
        <p>
          ¡Hola! Mi nombre es Juan Gajardo. Soy desarrollador fullstack con
          experiencia en React, Express JS, Node JS, MongoDB, .NET, SQL Server,
          AWS y GCP.
        </p>

        <p>
          Mi objetivo principal es lograr tener una cartera propia de clientes y
          trabajar con la mayor responsabilidad y eficiencia posible.
        </p>

        <p>
          Estoy constantemente aprendiendo nuevas cosas, por lo que me considero
          un autodidacta de corazón con el fin de estar ganando más experiencia
          en el desarrollo de las tecnologías.
        </p>

        <p>
          Mi último proyecto fue "Boleta electrónica TAS" para PDSSA Argentina,
          que fue realizado con React, Node y para el diseño utilicé Material
          UI.
        </p>

        <p>
          Siempre estoy subiendo nuevos proyectos de aprendizaje a mi
          repositorio GIT en Gitlab. Si quieres revisarlos pincha{" "}
          <a
            href="https://gitlab.com/jgajardon"
            target="_blank"
            rel="noopener noreferrer"
          >
            acá
          </a>
        </p>
      </Content>
    </div>
  );
}

export default AboutPage;
