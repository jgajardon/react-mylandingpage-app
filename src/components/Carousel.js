import React from "react";

import Card from "../components/Card";
import Modal from "../components/Modal";

import sicImage from "../assets/images/sic.png";
import sic_1 from "../assets/images/sic_1.png";
import sic_2 from "../assets/images/sic_2.png";
import tasImage from "../assets/images/tas.png";
import mppImage from "../assets/images/mpp.png";
import mpp_1 from "../assets/images/mpp_1.png";
import mpp_2 from "../assets/images/mpp_2.png";
import inkalendareImage from "../assets/images/inkalendare.png";
import ink_1 from "../assets/images/ink_1.png";
import ink_2 from "../assets/images/ink_2.png";
import coronavirusImage from "../assets/images/coronavirus.png";
import covid_1 from "../assets/images/covid_1.png";
import covid_2 from "../assets/images/covid_2.png";
import notFoundImage from "../assets/images/sin_imagen.png";
import Container from "react-bootstrap/Container";
import { Row, Col, Image, Button } from "react-bootstrap";

import SlideCarousel from "./Slide";
import Job from "./Job";

class Carousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      job: {
        title: "",
      },
      items: [
        {
          id: 0,
          title: "Boleta electronica TAS",
          subTitle: "Para PDSSA",
          description:
            "Sistema desarrollado para el control y visualizacion de las boletas electronicas emitidas por los clientes, se desarrollo modulo dashboard para la visualizacion de datos del cliente, modulo de busqueda de transacciones y boletas, carga de certificados por archivo, descarga de reportes en formato excel.",
          technologies: [
            {
              technology: "Javascript",
              variant: "warning",
            },
            {
              technology: "Node",
              variant: "success",
            },
            {
              technology: "React",
              variant: "info",
            },
            {
              technology: "Docker",
              variant: "secondary",
            },
          ],
          imgSrc: tasImage,
          image1: notFoundImage,
          image2: notFoundImage,
          link: "http://pdssa.com.ar/",
          selected: false,
        },
        {
          id: 1,
          title: "Sistema integración concesionario",
          subTitle: "Para crossover",
          description:
            "Sistema desarrollado para el control y gestion de las ventas, servicios y repuestos realizadas por los concesionarios de indumotora, contiene un modulo de de busqueda con multiples filtros enlazados, descarga de reporte con los filtros aplicados, carga de archivo excel o csv con ventas, dashboard para visualizar datos.",
          technologies: [
            {
              technology: "Javascript",
              variant: "warning",
            },
            {
              technology: "Node",
              variant: "success",
            },
            {
              technology: "React",
              variant: "info",
            },
            {
              technology: "SQL Server",
              variant: "secondary",
            },
          ],
          imgSrc: sicImage,
          image1: sic_1,
          image2: sic_2,
          link: "http://crossoverconsulting.cl/",
          selected: false,
        },
        {
          id: 2,
          title: "Mantenciones KIA",
          subTitle: "Para indumotora",
          description:
            "Sistema desarrollado para la gestion de mantenciones realizadas a los vehiculos en los diferentes concesionarios de Kia, modulo de dashboard, mantenedor de mantenciones, carga automatica y manual de mantenciones desde proveedor, proceso automatizado de pago de mantenciones a proveedor.",
          technologies: [
            {
              technology: "Javascript",
              variant: "warning",
            },
            {
              technology: "Node",
              variant: "success",
            },
            {
              technology: "React",
              variant: "info",
            },
            {
              technology: "SQL Server",
              variant: "secondary",
            },
          ],
          imgSrc: mppImage,
          image1: mpp_1,
          image2: mpp_2,
          link: "http://www.indumotora.cl/",
          selected: false,
        },
        {
          id: 3,
          title: "Tienda Inkalendare",
          subTitle: "Para Inkalendare",
          description:
            "E-commerce realizado para tienda Inkalendare dedicada a la venta de articulos para el cuidado personal.",
          technologies: [
            {
              technology: "Prestashop",
              variant: "info",
            },
            {
              technology: "AWS",
              variant: "warning",
            },
            {
              technology: "Mysql",
              variant: "secondary",
            },
            {
              technology: "WebPay",
              variant: "info",
            },
            {
              technology: "MercadoPago",
              variant: "secondary",
            },
          ],
          imgSrc: inkalendareImage,
          image1: ink_1,
          image2: ink_2,
          link: "http://www.inkalendare.cl/",
          selected: false,
        },
        {
          id: 4,
          title: "Covid-19 App",
          subTitle: "Para el mundo",
          description:
            "Sistema desarrollado para la gestion de mantenciones realizadas a los vehiculos en los diferentes concesionarios de Kia, modulo de dashboard, mantenedor de mantenciones, carga automatica y manual de mantenciones desde proveedor, proceso automatizado de pago de mantenciones a proveedor.",
          technologies: [
            {
              technology: "Javascript",
              variant: "warning",
            },
            {
              technology: "Node",
              variant: "success",
            },
            {
              technology: "React",
              variant: "info",
            },
            {
              technology: "SQL Server",
              variant: "secondary",
            },
          ],
          imgSrc: coronavirusImage,
          image1: covid_1,
          image2: covid_2,
          link: "http://app.coronavirus.jcgajardon.com/",
          selected: false,
        },
      ],
    };
  }

  handleCardClick = (id, card) => {
    let items = [...this.state.items];

    items[id].selected = items[id].selected ? false : true;

    items.forEach((item) => {
      if (item.id !== id) {
        item.selected = false;
      }
    });

    console.log(items);

    this.setState({
      items,
    });
  };

  makeItems = (items) => {
    return items.map((item) => {
      return (
        <Card
          item={item}
          click={(e) => this.handleCardClick(item.id, e)}
          key={item.id}
        />
      );
    });
  };

  selectJob = (id) => {
    const { items } = this.state;

    const selectedItem = items.find((x) => x.id === id);

    this.setState({ job: selectedItem, showModal: true });
  };

  closeModal = () => {
    console.log("close modal");
    this.setState({ showModal: false });
  };

  render() {
    const { showModal, items, job } = this.state;
    console.log(this.state.showModal);
    return (
      <Container>
        <h4 style={{ textAlign: "center" }}>Proyectos</h4>
        <Row>
          {items.map((item) => (
            <Col
              xs={6}
              md={4}
              style={{
                padding: "20px",
                justifyContent: "center",
                textAlign: "center",
              }}
            >
              <Image width="300px" height="150px" src={item.imgSrc} rounded />
              <div style={{ justifyContent: "center", textAlign: "center" }}>
                <h5>{item.title}</h5>
                <Button
                  variant="primary"
                  size="sm"
                  onClick={() => this.selectJob(item.id)}
                >
                  Ver mas
                </Button>{" "}
              </div>
            </Col>
          ))}
        </Row>
        <Modal show={showModal} closeModal={this.closeModal} title={job.title}>
          <Job job={job} />
        </Modal>
      </Container>
    );
  }
}

export default Carousel;
