import React, { useState } from "react";
import { Modal as ModalBootstrap, Button } from "react-bootstrap";

function Modal(props) {
  const { closeModal } = props;
  /* const [show, setShow] = useState(false); */

  /* const handleClose = () => setShow(false);
  const handleShow = () => setShow(true); */

  return (
    <ModalBootstrap size="lg" show={props.show} /* onHide={handleClose} */>
      <ModalBootstrap.Header>
        <ModalBootstrap.Title>{props.title}</ModalBootstrap.Title>
      </ModalBootstrap.Header>
      <ModalBootstrap.Body>{props.children}</ModalBootstrap.Body>
      <ModalBootstrap.Footer>
        <Button variant="secondary" onClick={closeModal}>
          Volver
        </Button>
      </ModalBootstrap.Footer>
    </ModalBootstrap>
  );
}

export default Modal;
