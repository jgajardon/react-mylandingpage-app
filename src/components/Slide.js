import React from "react";
import { Carousel, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

const SlideCarousel = (props) => {
  const { items } = props;
  console.log(props);
  return (
    <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12">
            <h3>Algunos de mis proyectos</h3>
          </div>
        </div>
        <div
          className="row"
          style={{ margin: "auto", justifyContent: "center" }}
        >
          <div className="col-8">
            <Carousel>
              {items.map((item) => (
                <Carousel.Item>
                  <img
                    className="d-block w-100"
                    src={item.imgSrc}
                    alt="First slide"
                  />
                  <Carousel.Caption>
                    <h3>{item.title}</h3>
                    <Button variant="primary">Ver mas</Button>{" "}
                  </Carousel.Caption>
                </Carousel.Item>
              ))}
            </Carousel>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SlideCarousel;
