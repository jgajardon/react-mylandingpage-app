import React from "react";
import { Container, Row, Col, Badge, Image } from "react-bootstrap";

const Job = (props) => {
  const {
    job: { title, description, technologies, subtitle, image1, image2, link },
  } = props;
  console.log(props);
  return (
    <Container>
      <Row style={{ textAlign: "center" }}>
        <Col>
          <p style={{ textAlign: "justify", textJustify: "inter-word" }}>
            {description}
          </p>
        </Col>
        <Col style={{ margin: "auto" }}>
          {technologies.map((technology) => (
            <Badge style={{ marginLeft: "10px" }} variant={technology.variant}>
              {technology.technology}
            </Badge>
          ))}
        </Col>
      </Row>
      <Row style={{ textAlign: "center" }}>
        <Col>
          <Image width="600px" height="200px" src={image1} fluid />
        </Col>
      </Row>
      <Row style={{ textAlign: "center", marginTop: "50px" }}>
        <Col>
          <Image width="600px" height="200px" src={image2} fluid />
        </Col>
      </Row>
    </Container>
  );
};

export default Job;
