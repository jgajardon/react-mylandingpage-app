import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

function Technologies(props) {
  const styleCol = {
    textAlign: "center",
    padding: "42px",
  };

  const styleIcon = {
    fontSize: "150px",
  };

  return (
    <Container fluid={true} style={{ textAlign: "center", padding: "40px" }}>
      <h4>Tecnologías</h4>
      <Row className="justify-content-around">
        <Col style={styleCol}>
          <i
            style={styleIcon}
            class="devicon-mongodb-plain-wordmark colored"
          ></i>
        </Col>
        <Col style={styleCol}>
          <i
            style={styleIcon}
            class="devicon-express-original-wordmark colored"
          ></i>
        </Col>
        <Col style={styleCol}>
          <i
            style={styleIcon}
            class="devicon-react-original-wordmark colored"
          ></i>
        </Col>
        <Col style={styleCol}>
          <i
            style={styleIcon}
            class="devicon-nodejs-plain-wordmark colored"
          ></i>
        </Col>
        <Col style={styleCol}>
          <i
            style={styleIcon}
            class="devicon-amazonwebservices-plain-wordmark colored"
          ></i>
        </Col>

        <Col style={styleCol}>
          <i style={styleIcon} class="devicon-csharp-plain colored"></i>
        </Col>
        <Col style={styleCol}>
          <i style={styleIcon} class="devicon-javascript-plain colored"></i>
        </Col>
        <Col style={styleCol}>
          <i
            style={styleIcon}
            class="devicon-electron-original-wordmark colored"
          ></i>
        </Col>
      </Row>
    </Container>
  );
}

export default Technologies;
