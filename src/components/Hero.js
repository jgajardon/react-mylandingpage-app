import React from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Button } from "react-bootstrap";

function Hero(props) {
  return (
    <Jumbotron className="bg-transparent jumbotron-fluid p-0">
      <Container fluid={true}>
        <Row className="justify-content-center py-5">
          <Col md={8} sm={12}>
            {props.title && (
              <h1
                style={{ whiteSpace: "pre-wrap" }}
                className="display-1 font-weight-bolder"
              >
                {props.title}
              </h1>
            )}
            {props.subTitle && (
              <h3 className="display-4 font-weight-light">{props.subTitle}</h3>
            )}
            {props.text && (
              <div>
                <h3 className="lead font-weight-light">{props.text}</h3>
                <Button
                  variant="danger"
                  onClick={() =>
                    window.open(
                      "https://www.dropbox.com/s/n48ugzrypmb1jmm/resume.pdf?dl=0",
                      "_blank"
                    )
                  }
                >
                  Descargar CV
                </Button>
              </div>
            )}
          </Col>
        </Row>
      </Container>
    </Jumbotron>
  );
}

export default Hero;
