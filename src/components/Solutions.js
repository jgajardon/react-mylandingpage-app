import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { ReactComponent as WebDevelopment } from "../assets/svg/web_development.svg";
import { ReactComponent as MobileDevelopment } from "../assets/svg/mobile_development.svg";
import { ReactComponent as CloudDevelopment } from "../assets/svg/cloud_development.svg";
import { ReactComponent as Integrations } from "../assets/svg/integrations.svg";

function Solutions(props) {
  const styleCol = {
    textAlign: "center",
    padding: "42px",
  };

  const styleImage = {
    width: "50%",
    height: "50%",
  };

  return (
    <Container fluid={true} style={{ textAlign: "center", padding: "40px" }}>
      <h4>Servicios</h4>
      <Row className="justify-content-around">
        <Col style={styleCol}>
          <h5>Desarrollo de software</h5>
          <WebDevelopment style={{ width: "200px", height: "200px" }} />
          <p>Desarrollamos software a la medida con base en tus ideas</p>
        </Col>
        <Col style={styleCol}>
          <h5>Desarrollo móvil</h5>
          <MobileDevelopment style={{ width: "200px", height: "200px" }} />
          <p>Realizamos tu idea móvil a la medida</p>
        </Col>
        <Col style={styleCol}>
          <h5>Computación en la nube</h5>
          <CloudDevelopment style={{ width: "200px", height: "200px" }} />
          <p>Utilizamos la vanguardia de la nube para alojar tus ideas</p>
        </Col>
        <Col style={styleCol}>
          <h5>Integraciones</h5>
          <Integrations style={{ width: "200px", height: "200px" }} />
          <p>Tenemos la solución, si necesitas conectarte con otros sistemas</p>
        </Col>
      </Row>
    </Container>
  );
}

export default Solutions;
