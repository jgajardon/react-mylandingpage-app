import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Button } from "react-bootstrap";
import "./App.css";

import Footer from "./components/Footer";
import HomePage from "./pages/HomePage";
import AboutPage from "./pages/AboutPage";
import ContactPage from "./pages/ContactPage";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "Garrett Love",
      headerLinks: [
        { title: "Home", path: "/" },
        { title: "About", path: "/about" },
        { title: "Contact", path: "/contact" },
      ],
      home: {
        title: "Dev",
        subTitle: "Preocupación por los proyectos",
        text: "¿Necesitas un software? Revisa mi perfil",
      },
      about: {
        title: "Acerca de mí",
      },
      contact: {
        title: "Hola",
      },
    };
  }

  //

  render() {
    return (
      <Router>
        <Container className="p-0" fluid={true}>
          <Navbar className="border-bottom" bg="transparent" expand="lg">
            <Navbar.Brand>
              JGajardon{" "}
              <i
                onClick={() =>
                  window.open(
                    "https://www.linkedin.com/in/jcgajardon/",
                    "_blank"
                  )
                }
                target="_blank"
                style={{ cursor: "pointer" }}
                class="devicon-linkedin-plain colored"
              ></i>{" "}
              <i
                onClick={() =>
                  window.open("https://gitlab.com/jgajardon", "_blank")
                }
                target="_blank"
                style={{ cursor: "pointer" }}
                class="devicon-gitlab-plain colored"
              ></i>
            </Navbar.Brand>
            <Navbar.Toggle className="border-0" aria-controls="navbar-toggle" />
            <Navbar.Collapse id="navbar-toggle">
              <Nav className="ml-auto">
                <Link className="nav-link" to="/">
                  Inicio
                </Link>
                <Link className="nav-link" to="/about">
                  Acerca de mí
                </Link>
                <Link className="nav-link" to="/contact">
                  Contacto
                </Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          <Route
            path="/"
            exact
            render={() => (
              <HomePage
                title={this.state.home.title}
                subTitle={this.state.home.subTitle}
                text={this.state.home.text}
              />
            )}
          />
          <Route
            path="/about"
            render={() => <AboutPage title={this.state.about.title} />}
          />
          <Route
            path="/contact"
            render={() => <ContactPage title={this.state.contact.title} />}
          />
          <Footer />
        </Container>
      </Router>
    );
  }
}

export default App;
